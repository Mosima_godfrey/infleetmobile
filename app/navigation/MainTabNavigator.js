import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation';

import LoginScreen from '../screens/Login';
import HomeScreen from '../screens/Home2';
import PurchaseOrderScreen from '../screens/PurchaseOder';

const config = Platform.select({
  web: { headerMode: 'screen' },
  default: {},
});

const LoginStack = createStackNavigator(
  {
    Login: LoginScreen,
    Home: HomeScreen,
    PurchaseOrder: PurchaseOrderScreen,
  },
  config
);

LoginStack.path = '';



export default LoginStack;
