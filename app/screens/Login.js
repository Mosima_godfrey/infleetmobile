import * as WebBrowser from 'expo-web-browser';
import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  Dimensions,
  ImageBackground
} from 'react-native';
import { Container, Content, Form, Item, Input, Button, Icon } from 'native-base';
import logo from '../../assets/images/splash.png';
import background from '../../assets/images/background_bmw.png';

export default function LoginScreen(props) {
  return (

    <ImageBackground
                source={background}
                style={styles.container}
    
            >
              
      <Content style={styles.input}>
      <View style={{ alignSelf: 'center' }}>
                    <Image source={logo} style={{ resizeMode: 'contain', marginTop: 10, width: Dimensions.get("window").width / 1.35, height: 130 }} />
      </View>
        <Form>
          <Item style={{backgroundColor:'#fff'}} rounded>
            <Icon active name='person' />
            <Input placeholder="Username" />
          </Item>
     
     
          <Item style={{backgroundColor:'#fff'}} rounded>
          <Icon active name='lock' />
            <Input placeholder="Password" />
          </Item>
          <View style={styles.button}>
            <Button onPress={() => props.navigation.navigate('Home')} danger full rounded><Text style={styles.helpLinkText}> Login </Text></Button>
          </View>
        </Form>
      </Content>
      {/* <View style={styles.overlay}/> */}
    </ImageBackground>
  );
}

LoginScreen.navigationOptions = {
  header: null,
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  input: {
    paddingTop: 70
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  button: {
    width: 300,
    alignItems: 'center',
    alignSelf: 'center',
    paddingTop: 25
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#fff',
  },
  itemInput:{
    paddingTop:20
  },            
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: 'white',
    opacity: 0.2
  }
});
