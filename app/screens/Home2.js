import React, { Component } from 'react';
import {
    ImageBackground,
    StyleSheet,
    View,
    Dimensions,
    TouchableOpacity,
    Image
} from 'react-native';
import {
    Button,
    Text
} from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';

import background from '../../assets/images/background_bmw.png';
import logo from '../../assets/images/splash.png';

class Home extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ImageBackground
                source={background}
                style={styles.container}
            >
                <View style={styles.overlay}></View>
                <View style={{ alignSelf: 'center' }}>
                    <Image source={logo} style={{ resizeMode: 'contain', marginTop: 10, width: Dimensions.get("window").width / 1.35, height: 130 }} />
                </View>


                <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center', alignItems: 'center', paddingTop: 5, paddingHorizontal: 10 }}>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('PurchaseOrder') }}>
                        <Image source={require('../../assets/images/logo.jpg')} style={styles.actionIcons} />
                        <Text style={styles.actionIconText}>ORDERS</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('PurchaseOrder') }}>
                        <Image source={require('../../assets/images/logo.jpg')} style={styles.actionIcons} />
                        <Text style={styles.actionIconText}>Quotes</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('PurchaseOrder') }}>
                        <Image source={require('../../assets/images/logo.jpg')} style={styles.actionIcons} />
                        <Text style={styles.actionIconText}>INVOICES</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('PurchaseOrder') }}>
                        <Image source={require('../../assets/images/logo.jpg')} style={styles.actionIcons} />
                        <Text style={styles.actionIconText}>REBILLS</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('PurchaseOrder') }}>
                        <Image source={require('../../assets/images/logo.jpg')} style={styles.actionIcons} />
                        <Text style={styles.actionIconText}>SERVICES</Text>
                    </TouchableOpacity>



                </View>
                <TouchableOpacity
                    onPress={() => { this.props.navigation.navigate('Login') }}
                >
                    <Text style={{ marginBottom: 15, textAlign: 'center', color: 'rgba(255,0,0,1)' }}> <Icon name='md-arrow-back' color='rgba(255,0,0,1)' size={16} /> LOGOUT</Text>
                </TouchableOpacity>


            </ImageBackground>
        )
    }
}
Home.navigationOptions = {
    header: null,
};

const styles = StyleSheet.create({
    container: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,

    },
    actionIcons: {
        width: 75,
        height: 75,
        margin: 5,
        marginHorizontal: 20,
    },
    actionIconText: {
        color: 'black',
        fontSize: 12,
        textAlign: 'center',
        marginBottom: 10,
        fontWeight: 'bold'
    },
    overlay: {
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        backgroundColor: 'white',
        opacity: 0.7
    }
});

export default Home;