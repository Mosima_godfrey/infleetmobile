import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text } from 'react-native';
import { Container, Input, Content, Form, Item, Picker, Icon } from 'native-base';

export default class PurchaseOrderScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected2: undefined
    };
  }
  onValueChange2(value) {

    if (value !== 'key0') {
      this.setState({
        selected2: value
      });
    }

  }
  render() {
    return (
      <Container>
        <Content>
          <Form>
            <Item picker>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                iosHeader={"Purchase Order Type"}
                style={{ width: undefined }}
                placeholderStyle={{ color: "#007aff" }}
                placeholderIconColor="#007aff"
                selectedValue={this.state.selected2}
                onValueChange={this.onValueChange2.bind(this)}
                placeholder="Purchase Order Type"
              >
                <Picker.Item style={{backgroundColor:'red'}} label="Purchase Order Type" value="key0" />
                <Picker.Item label="Services" value="key1" />
                <Picker.Item label="Tyres" value="key2" />
                <Picker.Item label="Towing" value="key3" />
                <Picker.Item label="Repairs" value="key4" />
                <Picker.Item label="Panel Beaters" value="key5" />
                <Picker.Item label="Assessor" value="key6" />
              </Picker>
            </Item>
          <Item rounded>
            <Icon active name='home' />
            <Input placeholder='Icon Textbox'/>
          </Item>
     
          <Item rounded>
            <Input placeholder='Icon Alignment in Textbox'/>
            <Icon active name='swap' />
          </Item>
          </Form>
        </Content>
      </Container>
    );
  }
}


PurchaseOrderScreen.navigationOptions = {
  title: 'Purchase Order',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});